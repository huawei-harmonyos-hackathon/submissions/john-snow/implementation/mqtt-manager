# MQTT Manager
MQTT Manager Service

## Description
The application runs as a system service that interacts with MQTT broker to expose product specific function on DBus for other services or applications on the target system.   
Most of the code is product independent except the mqm-adapter modules found in source directory. This module should implement the product specific code and post fuction states on DBUS via mqm-executer in the main application event loop.    
Each product should adapt the MQTT Manager profile as the gateway to the MQTT Broker. The functions values can be get and set from DBus using standard interfaces by other services which in turn do the product specific logic. See as example:   
https://gitlab.com/harmonyos-hackathon/resources/smart-lamp-demo

## Dependencies
| Component | Source |
| ------ | ------ |
| GLib | https://wiki.gnome.org/Projects/GLib |

## Build
mkdir build && cd build    
cmake ..
make

## Install
make install    

## Build options
| Option | Default | Description |
| ------ | ------ | ------ |
| CONFDIR | /etc | Default configuration directory |
| SYSTEMD | true | Enable systemd watchdog if run as service |
