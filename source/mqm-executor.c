/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 * @file mqm-executor.c
 */

#include "mqm-executor.h"
#include "mqm-adapter.h"
#include "mqm-dbusown.h"

gboolean g_lamp_switch = FALSE;

/**
 * @brief Post new event
 * @param executor A pointer to the executor object
 * @param type The type of the new event to be posted
 * @param payload Action payload
 */
static void
post_executor_event(MqmExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload);
/**
 * @brief GSource prepare function
 */
static gboolean executor_source_prepare(GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean executor_source_dispatch(GSource *source, GSourceFunc callback, gpointer _executor);
/**
 * @brief GSource callback function
 */
static gboolean executor_source_callback(gpointer _executor, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void executor_source_destroy_notify(gpointer _executor);
/**
 * @brief Async queue destroy notification callback function
 */
static void executor_queue_destroy_notify(gpointer _executor);
/**
 * @brief Process lamp switch state from mqtt
 */
static void do_update_lamp_state_event(MqmExecutor *executor, const gchar *state);
/**
 * @brief Process lamp switch state from dbus
 */
static void do_update_dbus_lamp_state_event(MqmExecutor *executor, const gchar *state);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs executor_source_funcs = {
    executor_source_prepare,
    NULL,
    executor_source_dispatch,
    NULL,
    NULL,
    NULL,
};

static void
post_executor_event(MqmExecutor *executor, ExecutorEventType type, ExecutorActionPayload payload)
{
    MqmExecutorEvent *e = NULL;

    g_assert(executor);

    e = g_new0(MqmExecutorEvent, 1);
    e->type = type;
    e->payload = payload;
    g_async_queue_push(executor->queue, e);
}

static gboolean executor_source_prepare(GSource *source, gint *timeout)
{
    MqmExecutor *executor = (MqmExecutor *) source;

    MQM_UNUSED(timeout);

    return (g_async_queue_length(executor->queue) > 0);
}

static gboolean executor_source_dispatch(GSource *source, GSourceFunc callback, gpointer _executor)
{
    MqmExecutor *executor = (MqmExecutor *) source;
    gpointer event = g_async_queue_try_pop(executor->queue);

    MQM_UNUSED(callback);
    MQM_UNUSED(_executor);

    if (event == NULL)
        return G_SOURCE_CONTINUE;

    return executor->callback(executor, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean executor_source_callback(gpointer _executor, gpointer _event)
{
    MqmExecutor *executor = (MqmExecutor *) _executor;
    MqmExecutorEvent *event = (MqmExecutorEvent *) _event;

    g_assert(executor);
    g_assert(event);

    switch (event->type) {
    case EXECUTOR_UPDATE_LAMP_STATE:
        do_update_lamp_state_event(executor, event->payload.lamp_state);
        break;
    case EXECUTOR_UPDATE_DBUS_LAMP_STATE:
        do_update_dbus_lamp_state_event(executor, event->payload.lamp_state);
        break;
    default:
        g_warning("Executor unknown event='%d'", event->type);
        break;
    }

    g_free(event->payload.lamp_state);
    g_free(event);

    return TRUE;
}

static void do_update_lamp_state_event(MqmExecutor *executor, const gchar *state)
{
    MqmDBusOwn *dbusown = NULL;
    MqmAdapter *adapter = NULL;

    g_assert(executor);
    g_assert(state);

    dbusown = (MqmDBusOwn *) executor->dbusown;
    g_debug("Executor update lamp state to LampState='%s'", state);

    if (g_strcmp0(state, MQM_LAMP_STATE_ON) != 0)
        g_lamp_switch = FALSE;
    else
        g_lamp_switch = TRUE;

    mqm_dbusown_emit_new_lamp_state(dbusown, state);
}

static void do_update_dbus_lamp_state_event(MqmExecutor *executor, const gchar *state)
{
    MqmDBusOwn *dbusown = NULL;
    MqmAdapter *adapter = NULL;

    g_assert(executor);
    g_assert(state);

    dbusown = (MqmDBusOwn *) executor->dbusown;
    adapter = (MqmAdapter *) executor->mqtt_adapter;
    g_debug("Executor update lamp state to LampState='%s'", state);

    if (g_strcmp0(state, MQM_LAMP_STATE_ON) != 0)
        g_lamp_switch = FALSE;
    else
        g_lamp_switch = TRUE;

    mqm_adapter_set_switch_state(adapter, state);
    mqm_dbusown_emit_new_lamp_state(dbusown, state);
}

static void executor_source_destroy_notify(gpointer _executor)
{
    MqmExecutor *executor = (MqmExecutor *) _executor;

    g_assert(executor);
    g_debug("Executor destroy notification");

    mqm_executor_unref(executor);
}

static void executor_queue_destroy_notify(gpointer _executor)
{
    MQM_UNUSED(_executor);
    g_debug("Executor queue destroy notification");
}

MqmExecutor *mqm_executor_new(MqmOptions *options)
{
    MqmExecutor *executor
        = (MqmExecutor *) g_source_new(&executor_source_funcs, sizeof(MqmExecutor));

    g_assert(executor);

    g_ref_count_init(&executor->rc);
    executor->callback = executor_source_callback;
    executor->options = mqm_options_ref(options);
    executor->queue = g_async_queue_new_full(executor_queue_destroy_notify);

    g_source_set_callback(
        MQM_EVENT_SOURCE(executor), NULL, executor, executor_source_destroy_notify);
    g_source_attach(MQM_EVENT_SOURCE(executor), NULL);

    return executor;
}

MqmExecutor *mqm_executor_ref(MqmExecutor *executor)
{
    g_assert(executor);
    g_ref_count_inc(&executor->rc);

    return executor;
}

void mqm_executor_unref(MqmExecutor *executor)
{
    g_assert(executor);

    if (g_ref_count_dec(&executor->rc) == TRUE) {
        if (executor->options != NULL)
            mqm_options_unref(executor->options);

        if (executor->dbusown != NULL)
            mqm_dbusown_unref((MqmDBusOwn *) executor->dbusown);

        if (executor->mqtt_adapter != NULL)
            mqm_adapter_unref((MqmAdapter *) executor->mqtt_adapter);

        g_async_queue_unref(executor->queue);
        g_source_unref(MQM_EVENT_SOURCE(executor));
    }
}

void mqm_executor_set_dbusown(MqmExecutor *executor, gpointer dbusown)
{
    g_assert(executor);
    g_assert(dbusown);

    executor->dbusown = mqm_dbusown_ref((MqmDBusOwn *) dbusown);
}

void mqm_executor_set_mqtt_adapter(MqmExecutor *executor, gpointer mqtt_adapter)
{
    g_assert(executor);
    g_assert(mqtt_adapter);

    executor->mqtt_adapter = mqm_adapter_ref((MqmAdapter *) mqtt_adapter);
}

void mqm_executor_push_event(MqmExecutor *executor,
                             ExecutorEventType type,
                             ExecutorActionPayload payload)
{
    post_executor_event(executor, type, payload);
    g_main_context_wakeup(NULL);
}
